package org.akent.listviewswitchtest;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListWithSwitch extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(android.R.id.content, new ListWithSwitchFragment())
                    .commit();
        }
    }

    public static class ListWithSwitchFragment extends Fragment {

        private Adapter _adapter;
        private ListView _primary;
        private ArrayList<JSONObject> _events = new ArrayList<JSONObject>();

        public ListWithSwitchFragment() {
        }

        class Adapter extends ArrayAdapter<JSONObject> {
            LayoutInflater _inflater;

            public Adapter(Context ctx) {
                super(ctx, R.layout.event_with_switch, R.id.text, _events);
                _inflater = LayoutInflater.from(getContext());
            }

            @Override
            public View getView(final int i, View v, ViewGroup parent) {
                if (v == null) {
                    v = _inflater.inflate(R.layout.event_with_switch, parent, false);
                }

                TextView tv = (TextView) v.findViewById(R.id.event_label);
                JSONObject o = getItem(i);
                tv.setText(o.optString("text"));
                return v;
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.listview, container, false);

            _primary = (ListView)rootView.findViewById(R.id.listview);
            _adapter = new Adapter(getActivity().getBaseContext());
            _primary.setAdapter(_adapter);

            for (int i = 0; i < 10; i++) {
                try {
                    _adapter.add(new JSONObject().put("text", "Event " + i));
                } catch (JSONException e) {}
            }

            _primary.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(getActivity(), "On item " + position + " clicked!",
                            Toast.LENGTH_SHORT).show();
                }
            });

            return rootView;
        }
    }
}
